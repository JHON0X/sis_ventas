-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-04-2019 a las 02:59:06
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `shop`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reg_cli`
--

CREATE TABLE `reg_cli` (
  `id` int(200) NOT NULL,
  `user` varchar(200) NOT NULL,
  `pass` varchar(200) NOT NULL,
  `names` text NOT NULL,
  `email` varchar(200) NOT NULL,
  `dirt` varchar(200) NOT NULL,
  `phone` int(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reg_cli`
--

INSERT INTO `reg_cli` (`id`, `user`, `pass`, `names`, `email`, `dirt`, `phone`) VALUES
(1, '', '', '', '', '', 0),
(2, '', '', '', '', '', 0),
(3, '', '', '', '', '', 0),
(4, 'jaja', 'jej', '', 'jiji', 'juju', 0),
(5, 'jaja', 'jiji', 'jeje', 'jojo', 'jujus', 0),
(6, 'jaja', 'jeje', 'JIJI', 'jojo', 'jujusas', 0),
(7, 'jajasj', 'Âºkaksksak', 'hola', 'email', 'cocha', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reg_item`
--

CREATE TABLE `reg_item` (
  `id` int(200) NOT NULL,
  `name_item` text NOT NULL,
  `precio` int(200) NOT NULL,
  `type_item` text NOT NULL,
  `description_item` varchar(200) NOT NULL,
  `img` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reg_item`
--

INSERT INTO `reg_item` (`id`, `name_item`, `precio`, `type_item`, `description_item`, `img`) VALUES
(1, '', 77, 'proteccion', 'usalo con Ã±eque', ''),
(2, '', 7451, 'proteccion', 'usalo con Ã±eque', 'Desert.jpg');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `reg_cli`
--
ALTER TABLE `reg_cli`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reg_item`
--
ALTER TABLE `reg_item`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `reg_cli`
--
ALTER TABLE `reg_cli`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `reg_item`
--
ALTER TABLE `reg_item`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
